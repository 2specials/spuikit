# SPUIKit
This repository contains a set of useful utilities about Apple development based on Apple UIKit framework.

The documentation can be found at https://2specials.gitlab.io/spuikit.

To generate documentation type the following command:
`jazzy --config .jazzy.json`