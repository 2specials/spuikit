//
//  SPUIUtility.swift
//  SPUIKit
//
//  Created by Federico on 29/10/2019.
//  Copyright © 2019 Federico Polesello. All rights reserved.
//

import Foundation
import UIKit
import SPFoundation

public class SPUIUtility {
    
    class func printFonts() {
        let fontFamilyNames = UIFont.familyNames
        for familyName in fontFamilyNames {
            SPFUtility.log(message: "------------------------------")
            SPFUtility.log(message: "Font Family Name = [\(familyName)]")
            let names = UIFont.fontNames(forFamilyName: familyName )
            SPFUtility.log(message: "Font Names = [\(names)]")
        }
    }
    
}
