
import Foundation
import UIKit

public extension UIAlertController {
 
    static func present(toViewController: UIViewController, title: String?, message: String?, actions: [UIAlertAction]) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        actions.forEach { (action) in
            alert.addAction(action)
        }
        DispatchQueue.main.async { toViewController.present(alert, animated: true, completion: nil) }
    }
    
}
