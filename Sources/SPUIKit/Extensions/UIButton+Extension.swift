
import UIKit

public extension UIButton {
    
    /**
     Sets the background image with the given color
     - parameter color: the color to set
     - parameter state: the state for which to set the color
     - parameter animated: if true an animation will be played
     - parameter duration: the animation's duration
     */
    func setBackgroundImage(withColor color:UIColor, for state:UIControl.State, animated:Bool = false, duration:TimeInterval = 0.2) {
        guard animated else {
            self.setBackgroundImage(UIImage(color: color), for: state)
            return
        }
        UIView.animate(withDuration: duration) {
            self.setBackgroundImage(UIImage(color: color), for: state)
        }
    }

    /**
     Sets the given image for all the UIButton's states
     - parameter image: the image to set
     - parameter renderingMode: the image rendering mode. Defaults to `.alwaysTemplate`
     */
    func setImageForAllStates(_ image:UIImage?, renderingMode: UIImage.RenderingMode = .alwaysTemplate) {
        self.setImage(image?.withRenderingMode(renderingMode), for: .normal)
        self.setImage(image?.withRenderingMode(renderingMode), for: .disabled)
        self.setImage(image?.withRenderingMode(renderingMode), for: .highlighted)
        self.setImage(image?.withRenderingMode(renderingMode), for: .selected)
        if #available(iOS 9.0, *) {
            self.setImage(image?.withRenderingMode(renderingMode), for: .focused)
        } else {
            // Fallback on earlier versions
        }
    }

    /**
     Sets the given color as titlecolor for all the UIButton's states
     - parameter color: the color to set
     */
    func setTitleColorForAllState(color:UIColor) {
        self.setTitleColor(color, for: .normal)
        self.setTitleColor(color, for: .disabled)
        self.setTitleColor(color, for: .highlighted)
        self.setTitleColor(color, for: .selected)
        if #available(iOS 9.0, *) {
            self.setTitleColor(color, for: .focused)
        } else {
            // Fallback on earlier versions
        }
    }

    /**
     Sets the given text as title (or attributedTitle in case of `underlined` text) for all the UIButton's states.
     - parameter text: the text to set
     - parameter uppercased: if `true`, the text will be uppercased
     - parameter underlined: if `true`, the text will be underlined
     */
    func setTitleForAllStates(_ text:String?, uppercased:Bool = false, underlined:Bool = false) {
        let title = !uppercased ? text : text?.uppercased()
        if underlined {
            let attributedTitle = NSAttributedString(string: title ?? "",
                                                     attributes: [.underlineStyle: NSUnderlineStyle.single.rawValue,
                                                                  .font: self.titleLabel?.font as Any])
            self.setAttributedTitle(attributedTitle, for: .normal)
            self.setAttributedTitle(attributedTitle, for: .disabled)
            self.setAttributedTitle(attributedTitle, for: .highlighted)
            self.setAttributedTitle(attributedTitle, for: .selected)
            if #available(iOS 9.0, *) {
                self.setAttributedTitle(attributedTitle, for: .focused)
            } else {
                // Fallback on earlier versions
            }
        }
        else {
            self.setTitle(title, for: .normal)
            self.setTitle(title, for: .disabled)
            self.setTitle(title, for: .highlighted)
            self.setTitle(title, for: .selected)
            if #available(iOS 9.0, *) {
                self.setTitle(title, for: .focused)
            } else {
                // Fallback on earlier versions
            }
        }
    }
    
    func actionHandler(controlEvents control: UIControl.Event,
                       forAction action:@escaping () -> Void) {
        self.actionHandler(action: action)
        self.addTarget(self, action: #selector(triggerActionHandler), for: control)
    }
    
    private func actionHandler(action:(() -> Void)? = nil) {
        struct __ { static var action :(() -> Void)? }
        if action != nil { __.action = action }
        else { __.action?() }
    }
    
    @objc private func triggerActionHandler() {
        self.actionHandler()
    }
    
}

