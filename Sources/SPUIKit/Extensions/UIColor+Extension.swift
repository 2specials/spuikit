
import Foundation
import UIKit

public extension UIColor {
    
    func convertToImage() -> UIImage {
        let rect : CGRect = CGRect(x: 0, y: 0, width: 1, height: 1)
        UIGraphicsBeginImageContext(rect.size)
        let context : CGContext = UIGraphicsGetCurrentContext()!
        
        context.setFillColor(self.cgColor)
        context.fill(rect)
        
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
    
    func as1ptImage(height:Int) -> UIImage {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: height))
        let ctx = UIGraphicsGetCurrentContext()
        self.setFill()
        if let ctx = ctx {
            ctx.fill(CGRect(x: 0, y: 0, width: 1, height: height))
            let image = UIGraphicsGetImageFromCurrentImageContext()
            if let image = image {
                UIGraphicsEndImageContext()
                return image
            }
        }
        return UIImage()
    }
    
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        getRed(&r, green: &g, blue: &b, alpha: &a)
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        return String(format:"#%06x", rgb)
    }
    
}
