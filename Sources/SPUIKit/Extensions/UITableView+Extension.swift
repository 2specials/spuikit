
import UIKit

public extension UITableView {
    
    /**
     Registers a nib object containing a cell with the table view under a specified identifier.
     Both the nib name and the identifier are infered from the class name, so there must be a nib available under that name.
     However you can override the `nib` function (see `NibLoadable`) to provide a custom nib.
     Same goes for the identifier (see `Reusable`).

     - seealso:
     [UITableView documentation](https://developer.apple.com/documentation/uikit/uitableview/1614937-register)
     - parameter cellClass: the class
     - parameter bundle: an optional bundle in which to look for the nib file. Pass nil (default) to use the main bundle.
     */
    func register<T:UITableViewCell>(_ cellClass:T.Type, bundle:Bundle? = nil) where T:SPUIReusable & SPUINibLoadable {
        self.register(cellClass.nib(bundle: nil), forCellReuseIdentifier: T.reuseIdentifier)
    }

    /**
     Registers a class for use in creating new table cells.
     The identifier is infered from the class name (see `Reusable`).

     - seealso:
     [UITableView documentation](https://developer.apple.com/documentation/uikit/uitableview/1614888-register)
     - parameter cellClass: the class
     - parameter bundle: an optional bundle in which to look for the nib file. Pass nil (default) to use the main bundle.
     */
    func register<T:UITableViewCell>(_ cellClass:T.Type, bundle:Bundle? = nil) where T:SPUIReusable {
        self.register(cellClass, forCellReuseIdentifier: T.reuseIdentifier)
    }

    /**
     Returns a reusable table-view cell object for the specified reuse identifier and adds it to the table.

     - seealso:
     [UITableView documentation](https://developer.apple.com/documentation/uikit/uitableview/1614878-dequeuereusablecell)
     - parameter indexPath: see `UITableView` documentation
     */
    func dequeueReusableCell<T:UITableViewCell>(indexPath: IndexPath) -> T? where T:SPUIReusable {
        return self.dequeueReusableCell(withIdentifier: T.reuseIdentifier, for: indexPath) as? T
    }
    
}

public extension UITableView {
    
    /**
     Call it to add a refresh control to the table

     - parameter refreshControl: the refresh control to add. Defaults to a normal UIRefreshControl
     - parameter action: the action to trigger
     - parameter target: the target
     */
    func setupRefreshControl(refreshControl:UIRefreshControl = UIRefreshControl(),
                                    action:Selector,
                                    target:Any?) {
        refreshControl.addTarget(target, action: action, for: .valueChanged)
        if #available(iOS 10.0, *) {
            self.refreshControl = refreshControl
        } else {
            // Fallback on earlier versions
        }
    }
    
}
