
import UIKit

typealias SPUIGradientPoints = (startPoint: CGPoint, endPoint: CGPoint)

enum SPUIGradientOrientation {
    case topRightBottomLeft
    case topLeftBottomRight
    case horizontal
    case vertical
    
    var startPoint : CGPoint {
        return points.startPoint
    }
    
    var endPoint : CGPoint {
        return points.endPoint
    }
    
    var points : SPUIGradientPoints {
        switch self {
        case .topRightBottomLeft:
            return (CGPoint(x: 0.0,y: 1.0), CGPoint(x: 1.0,y: 0.0))
        case .topLeftBottomRight:
            return (CGPoint(x: 0.0,y: 0.0), CGPoint(x: 1,y: 1))
        case .horizontal:
            return (CGPoint(x: 0.0,y: 0.5), CGPoint(x: 1.0,y: 0.5))
        case .vertical:
            return (CGPoint(x: 0.0,y: 0.0), CGPoint(x: 0.0,y: 1.0))
        }
    }
}

public extension UIView {
    
    func addBorder(color: UIColor,
                   width: CGFloat = 1.0,
                   cornerRadius: CGFloat = 0.0) {
        self.layer.borderWidth = width
        self.layer.borderColor = color.cgColor
        self.layer.cornerRadius = cornerRadius
    }
    
    func startRotating(duration: Double = 1.5) {
        let kAnimationKey = "rotation"
        if self.layer.animation(forKey: kAnimationKey) == nil {
            let animate = CABasicAnimation(keyPath: "transform.rotation")
            animate.duration = duration
            animate.repeatCount = Float.infinity
            animate.fromValue = 0.0
            animate.toValue = Float(Double.pi * 2.0)
            self.layer.add(animate, forKey: kAnimationKey)
        }
    }
    
    func stopRotating() {
        let kAnimationKey = "rotation"
        if self.layer.animation(forKey: kAnimationKey) != nil {
            self.layer.removeAnimation(forKey: kAnimationKey)
        }
    }
    
    func toImage() -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(bounds.size, false, UIScreen.main.scale)
        drawHierarchy(in: self.bounds, afterScreenUpdates: true)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }

    // Using a function since `var image` might conflict with an existing variable
    // (like on `UIImageView`)
    @available(iOS 10.0, *)
    func asImage() -> UIImage {
        let renderer = UIGraphicsImageRenderer(bounds: bounds)
        return renderer.image { rendererContext in
            layer.render(in: rendererContext.cgContext)
        }
    }
    
    func applyGradient(colours: [UIColor],
                       locations: [NSNumber]? = nil) {
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    internal func applyGradient(colours: [UIColor],
                                gradientOrientation: SPUIGradientOrientation) {
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.startPoint = gradientOrientation.startPoint
        gradient.endPoint = gradientOrientation.endPoint
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    func removeGradient() {
        if let subLayers = self.layer.sublayers {
            for subLayer in subLayers {
                if subLayer is CAGradientLayer {
                    subLayer.removeFromSuperlayer()
                }
            }
        }
    }
    
    /**
     Draws a shadow with the given parameters
     
     - parameter withColor: the color used for the shadow. Defaults to black
     - parameter offset: the offset used for the shadow. Defaults to `CGSize(width: 0, height: 5.0)`
     - parameter radius: the radius used for the shadow. Defaults to 8.0
     - parameter opacity: the opacity used for the shadow. 0.1
     */
    func applyShadow(withColor color:UIColor = .black,
                            offset:CGSize = CGSize(width: 0, height: 5.0),
                            radius:CGFloat = 8.0,
                            opacity:Float = 0.1) {
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = offset
        self.layer.shadowRadius = radius
        self.layer.shadowOpacity = opacity
        self.layer.masksToBounds = false
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
    }
    
    /**
     Creates and returns and instance of UIView looking for a nib file with name equal to the class name
     */
    @discardableResult
    static func instance<T>() -> T? {
        guard let instance = Bundle.main.loadNibNamed(String(describing: self),
                                                      owner: self,
                                                      options: nil)?.first as? T else {
            return nil
        }
        return instance
    }
    
}
