
import UIKit
import SPFoundation

public protocol SPUICoordinator: class {
    
    func start(presentationMode: SPFControllerPresentationMode)
    
    func start(fromController parentController: UIViewController?,
               withPresentationMode presentationMode: SPFControllerPresentationMode)
    
    func stop(closingMode: SPFControllerClosingMode,
              completionHandler: (() -> Void)?)
    
    func stop(fromController controller: UIViewController,
              withClosingMode closingMode: SPFControllerClosingMode,
              completionHandler: (() -> Void)?)
    
}

public extension SPUICoordinator {
    
    func start(presentationMode: SPFControllerPresentationMode) {}
    
    func start(fromController parentController: UIViewController?,
               withPresentationMode presentationMode: SPFControllerPresentationMode) {}
    
    func stop(closingMode: SPFControllerClosingMode,
              completionHandler: (() -> Void)?) {}
    
    func stop(fromController controller: UIViewController,
              withClosingMode closingMode: SPFControllerClosingMode,
              completionHandler: (() -> Void)?) {}
    
}
