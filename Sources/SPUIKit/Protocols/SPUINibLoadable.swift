
import Foundation
import UIKit

/**
 Protocol can be adopted by classes that can be instantiated from a nib
 */
public protocol SPUINibLoadable : class {
    
    /**
     Returns a nib file in the given bundle
     - parameter bundle: the bundle in which to look for the nib
     */
    static func nib(bundle: Bundle?) -> UINib
    
}

public extension SPUINibLoadable where Self: SPUIReusable {
    
    static func nib(bundle: Bundle?) -> UINib {
        return UINib(nibName: self.reuseIdentifier, bundle: bundle)
    }
    
}
