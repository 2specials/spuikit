
import Foundation

/**
 Protocol provides features useful for classes that needs to be reused
 */
public protocol SPUIReusable: class {
    
    /**
     A reuse identifier to identify the class
     */
    static var reuseIdentifier: String { get }
    
}

public extension SPUIReusable {
    
    static var reuseIdentifier: String {
        return String(describing: self.self)
    }
    
}
