
import Foundation
import UIKit

public protocol SPUIStoryboarded {
    
    static func instantiate(fromStoryboard storyboard: UIStoryboard) -> Self
    
}

public extension SPUIStoryboarded where Self: UIViewController {
    
    static func instantiate(fromStoryboard storyboard: UIStoryboard) -> Self {
        let id = String(describing: self)
        return storyboard.instantiateViewController(withIdentifier: id) as! Self
    }
    
}
