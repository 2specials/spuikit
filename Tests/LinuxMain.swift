import XCTest

import SPUIKitTests

var tests = [XCTestCaseEntry]()
tests += SPUIKitTests.allTests()
XCTMain(tests)
