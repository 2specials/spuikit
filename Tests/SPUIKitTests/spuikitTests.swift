import XCTest
@testable import SPUIKit

final class SPUIKitTests: XCTestCase {
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        // XCTAssertEqual(SPUIKit().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
    
}
